const express = require('express')
const mongoose = require('mongoose')
const User = require('../DB/User')
const route = express.Router();

// create new user with post request
route.post('/newUser', async(req,res)=>{
    const{firstName,lastName} = req.body;
    let user = {}
    user.firstName = firstName;
    user.lastName = lastName;
    let userModel = new User(user);
    await userModel.save();
    res.json(userModel);
})

// get all users
route.get('/allUsers', async(req,res)=>{
    User.find(function (err, usrs) {
            if (err) return console.error(err);
            res.json(usrs);
        })
})

module.exports = route;
