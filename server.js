const express = require('express')
const connectDB = require('./DB/Connection')

const app = express()
const port = 3000

// connetcting to MongoDB Atlas
connectDB()

app.use(express.json({extended:false}))
app.use('/api/userModel', require('./Api/User'))

app.listen(port, () => {
  console.log(`IoT-Workshop backend app listening at http://localhost:${port}`)
})